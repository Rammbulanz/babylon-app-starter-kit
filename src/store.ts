import { createStore, combineReducers, Store } from 'redux'
import { reducer as app } from './reducer/app'
import { reducer as lastAction } from './reducer/last-action'
import { TStoreState } from './types/TStoreState';

export const store: Store<TStoreState> = createStore(combineReducers({
	app,
	lastAction
}));


export function subscribe(callback: (state: TStoreState) => void) {
	return store.subscribe(() => callback(store.getState()));
}