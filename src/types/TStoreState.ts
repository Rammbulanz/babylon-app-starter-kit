import { App } from "../core/App";
import { AnyAction } from "redux";

export type TStoreState = {
	app: App
	lastAction: AnyAction
};