import { TransformNode } from "babylonjs";
import { Unsubscribe } from "redux";
import { TStoreState } from "src/types/TStoreState";
import { store } from "src/store";

export abstract class AbstractGameObject extends TransformNode {

	// Private members
	private _unsubscribe: Unsubscribe;

	// Properties

	public get listenToStore(): boolean {
		return !!this._unsubscribe;
	}
	public set listenToStore(value: boolean) {
		if (value != this.listenToStore) {
			if (value && !this._unsubscribe) {
				this._unsubscribe = store.subscribe(() => this.stateChanged(store.getState()));
				this.stateChanged(store.getState());
			} else if (!value && this._unsubscribe) {
				this._unsubscribe();
				this._unsubscribe = undefined;
			}
		}
	}

	/**
	 * Callback that is called, when the redux store has changed
	 */
	protected abstract stateChanged(state: TStoreState): void;

}