import { SceneLoader } from "babylonjs";
import { App } from "./App";

/**
 * This class is the entry point for the game logic.
 */
export class Game {

	public app: App;
	public promise: Promise<Game>;

	constructor(app: App) {
		this.app = app;
		this.promise = this.initialize();
	}

	public async initialize(): Promise<Game> {
		await SceneLoader.AppendAsync("assets/", "scene.babylon", this.app.scene);
		return this;
	}

}