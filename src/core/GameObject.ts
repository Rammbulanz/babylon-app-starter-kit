import { Color3, Node } from "babylonjs";
import { TStoreState } from "../types/TStoreState";
import { AbstractGameObject } from "./AbstractGameObject";

/**
 * This is the root instance for each item in scene
 */
export class GameObject extends AbstractGameObject {

	/**
	 * Set the outline values for each mesh within this instance
	 */
	public renderOutline(color: Color3, width?: number): void {
		for (const mesh of this.getChildMeshes(false)) {
			mesh.renderOutline = true;
			mesh.outlineColor = color;
			if (typeof width === "number") {
				mesh.outlineWidth = width;
			}
		}
	}

	/**
	 * Disable the outline values for each mesh within this instance
	 */
	public disableOutline(): void {
		for (const mesh of this.getChildMeshes(false)) {
			mesh.renderOutline = false;
		}
	}

	/**
	 * Set the overlay values for each mesh within this instance
	 */
	public renderOverlay(color: Color3, alpha?: number): void {
		for (const mesh of this.getChildMeshes(false)) {
			mesh.renderOverlay = true;
			mesh.overlayColor = color;
			if (typeof alpha === "number") {
				mesh.overlayAlpha = alpha;
			}
		}
	}

	/**
	 * Disable the overlay values for each mesh within this instance
	 */
	public disableOverlay(): void {
		for (const mesh of this.getChildMeshes(false)) {
			mesh.renderOverlay = false;
		}
	}

	protected stateChanged(state: TStoreState): void {

	}

}


Node.AddNodeConstructor("GameObject", (name, scene) => () => new GameObject(name, scene));